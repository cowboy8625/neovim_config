if has('win32')
    let g:python3_host_prog = '~/appdata/local/programs/python/python39/python.exe'
    let plugin_path = '~/.vim/plugged'
elseif  has('unix')
    let plugin_path = '~/.local/share/nvim/plugged'
endif

let g:netrw_winsize = 20

set incsearch
set spell spelllang=en_us
set tabstop=4 shiftwidth=4 expandtab
syntax on
syntax enable
let g:gruvbox_contrast_dark = 'hard'
if exists('+termguicolors')
    let &t_8f= "\<Esc>[38;2;%lu;%lu;&lum"
    let &t_8b= "\<Esc>[48;2;%lu;%lu;&lum"
endif

let g:gruvbox_invert_selection='0'


filetype plugin indent on

"autocmd BufWritePre *.py execute ':Black'
"nnoremap <silent> env :exe 'edit '.stdpath('config').'/init.vim'<CR>
nnoremap <silent> gf :Lex <CR>
tnoremap <silent> <ESC> <C-\><C-N>

call plug#begin(plugin_path)
Plug 'cespare/vim-toml'
Plug 'calviken/vim-gdscript3'
Plug 'jremmen/vim-ripgrep'
Plug 'tomtom/tcomment_vim'
Plug 'kannokanno/previm'
Plug 'morhetz/gruvbox'
Plug 'tpope/vim-surround'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'rust-lang-nursery/rustfmt'
"Plug 'ambv/black'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'tomtom/tcomment_vim'
call plug#end()

filetype plugin indent on

let g:airline_theme="gruvbox"
colorscheme gruvbox

nmap <F7> gg=G<C-o><C-o>
nmap <leader>rr <Plug>(coc-rename)
nnoremap <leader>prw :CocSearch <C-R>=expand("<cword>")<CR><CR>

nmap <Up> :echo "To move up use k"<CR>
imap <Up> <ESC><CR>
nmap <Down> :echo "To move down use j"<CR>
imap <Down> <ESC><CR>
nmap <Left> :echo "To move left use h"<CR>
imap <Left> <ESC><CR>
nmap <Right> :echo "To move right use l"<CR>
imap <Right> <ESC><CR>


set listchars=tab:>~,nbsp:_,trail:●
set list

highlight ColorColum ctermbg=magenta
call matchadd('ColorColum', '\%99v', 100)

function! Osc52Yank()
    let buffer=system('base64 -w0', @0) " -w0 to disable 76 char line wrapping
    let buffer='\ePtmux;\e\e]52;c;'.buffer.'\x07\e\\'
    silent exe "!echo -ne ".shellescape(buffer)." > ".shellescape(g:tty)
endfunction

nnoremap <leader>y :call Osc52Yank()<CR>

if has('win32') || has('win64')
    so ~\AppData\Local\nvim\coc.vim
endif
